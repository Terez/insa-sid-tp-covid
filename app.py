#!/usr/bin/env python3
#IMPORTING all necessary tools
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)#we get the info of what belongs to the app to find the resource- name is for a single module

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator - the ,,route” serves to inform what URL should start the function

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET'])#we use the HTTP method GET request, when accessing URL. Route only responds to GET message. Flask is automatically adds support for the HEAD according to HTTP

def index():
	response = render_template("menu.html",root=request.url_root, h="")##we define response, we render a template providing the name of the template “menu.html” and the variables 

	return response

	#case 2 hear message
@app.route('/<msg>', methods=['POST']) #we use the HTTP method POST the message to the client under conditions:

def add_heard(msg): #We add the message to the heard messages, in case the response 
                    #is received well and heard by the method POST. If not, the response gets the status Bad request.

	if client.catalog.add_message(Message(msg, Message.MSG_IRECEIVED)): 
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201)#Message received, new resource has been created
	else :
		reponse = Response(status=400) #else, bad request message (invalid syntax)
	return response
#End of person use case

#Hospital use case- The hospital uses the HTTP method GET and POST.
@app.route('/they_said_positive', methods=['GET','POST'])
def hospital(): #We define hospital if the request method equals to GET 
#or optionally to POST. If we use GET, the response is the one in the client catalog and the status is ok.
##So if the method is GET - we want see and get some information from the server

	if request.method == 'GET': #If we are requesting the list of covid messages (they said)
		client.catalog.purge(Message.MSG_POSITIVE)
		response = Response(client.catalog.c_export_type(Message.MSG_POSITIVE), mimetype='application/json', status=200) #we are exporting
        #by message type, a "success message" will be sent and the content transmitted 
    #Another option is the method POST. If the request is a new message in json, the request needs to be 
    #loaded and imported to the client catalog, with status created.
	elif request.method == 'POST': #to create a new message resource by sending it to the server
		if request.is_json:#if it's a json file
			req = json.loads(request.get_json())
			response = client.catalog.c_import_as_type(req, Message.MSG_POSITIVE)#we are importing the message
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201)#Message received
		else: ##If the response already exists in the catalog, the status is Bad request.
			response = Response(f"JSON expected", mimetype='text/plain', status=400)#else, bad request message (invalid syntax)
	else:#Any other method except POST and GET is forbidden client status error
		reponse = Response(f"forbidden method “{request.method}”.",status=403)#cannot access the content (client known but non authorized)
	return response
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True #If the debug is true, the server provides larger amount of data to avoid mistakes 
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
