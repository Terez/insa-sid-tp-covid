#!/usr/bin/env python3 - we import the file message and message catalog, we set the variables
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def _init_(self, catalog_path, debug = False, defaultProtocol="http://"): #we define the protocol http
		self.catalog = MessageCatalog(catalog_path)  #the self variable represents the instance of the object itself. 
		if debug: #In the Web.config file, locate the compilation element. Debugging is enabled when the debug attribute in the compilation element is set to true
			print(self.catalog)
		self.r = None #implementation of binary search tree
		self.protocol = defaultProtocol

	@classmethod #thanks to the function, we define the protocol. We take the url and change it to readable protocol for the server
	def withProtocol(cls, host): #use cls for the first argument to class methods
		res = host.find("://") > 1 # the host is text or chain of text. There is also a subchain.
		return res

	def completeUrl(self, host, route = ""): #If the client is not containing ://, the program uses http:// + 'localhost:5000'
		if not Client.withProtocol(host):
			host = self.protocol + host
		if route != "": #if it is the route, we define the HTTP and route
			route = "/"+route
		return host+route

	#send an I said message to a host
	def say_something(self, host): #we define a general empty message, we save it, add URL
		m = Message()
		self.catalog.add_message(m)
		route = self.completeUrl(host, m.content)
		self.r = requests.post(route) #in this point, we are dicectly asking the app.py to work
		if self.debug: #debug - we have more huge exctract of information to prevent mistakes
			print("POST  "+route + "→" + str(self.r.status_code)) #at the end, we make sure that server answered the client in the right status code
			print(self.r.text)
		return self.r.status_code == 201

	#add to catalog all the covid from host server
	def get_covid(self, host): #we add the url, ask the hospital by the method GET
		route = self.completeUrl(host,'/they_said_positive')
		self.r = requests.get(route)
		res = self.r.status_code == 200
		if res: #so the client gets back the list of answers. The client saves the data in json
			res = self.catalog.c_import(self.r.json())
		if self.debug:
			print("GET  "+ route + "→" + str(self.r.status_code))
			if res != False:
				print(str(self.r.json()))
		return res

	#send to server list of I sent messages
	def send_history(self, host): #the server adds the section '/they_said_positive'
		route = self.completeUrl(host,'/they_said_positive')
		self.catalog.purge(Message.MSG_ISENT) #the client internally deteles the data older than 14 days
		data = self.catalog.c_export_type(Message.MSG_ISENT)
		self.r = requests.post(route, json=data)
		if self.debug:
			print("POST  "+ route + "→" + str(self.r.status_code))
			print(str(data))
			print(str(self.r.text)) #the client creates a text document and sends it to server
		return self.r.status_code == 201 #makes sure that the server answered


if __name__ == "_main_": #the Client succesfully comunicates with the server, provides the functions, information, the data history etc.
	c = Client("client.json", True)
	c.say_something("localhost:5000")
	c.get_covid("localhost:5000")
	c.send_history("localhost:5000")
