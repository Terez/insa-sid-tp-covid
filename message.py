#!/usr/bin/env python3
import json
#import time
#replaced by the following two lines for testing purposes
import falsifytime
time = falsifytime.falsifytime()#FUNCTION 

#for generation of messages
from random import random
from hashlib import sha256

class Message:
	#the constructor can take various parameters
	#default is parameterless, it generates a message to be said
	#if type is not specified, it is to be imported from an export string or parsed export string
	
	#types of messages
     MSG_ISENT = 0  #to define (track) the category of sent messages
     MSG_IRECEIVED = 1#to define (track)the category of received messages
     MSG_POSITIVE = 2 #define (track)) the class of messages sent by positive people 
     #to the gathering "spot"  (received at hospital)
     def __init__(self, msg="", msg_type=False, msg_date=False): 
         if msg == "" : 
             self.content = Message.generate()
             self.type = Message.MSG_ISENT 
             self.date = time.time()
         elif msg_type is False :
             self.m_import(msg)
         else:
             self.content = msg
             self.type = msg_type
             if msg_date is False :
                 self.date = time.time() #otherwise, if date is False the time is system time
             else:
                 self.date = msg_date #else it message is a float, then the date should be this parameter
                
     def hdate(self): 
        return time.ctime(self.date)
            
	
     def age(self, days=True, as_string = False):#a method to compute the age of a message in days or in seconds
          age_msg = time.time() - self.date
          if days:
              age_msg = int(age_msg/(3600 * 24)) #return the mesg in days if it is given as a number 
          if as_string :
              d = int(age_msg/(3600 * 24))#return the mesg in days
              r = age_msg % (3600 * 24) #return the rest of the division for the conversion in days
              h = int(r/3600) #returns the rest of the division in hours if the rest is < 1 day
              r = r%3600 #returns the rest of the division  if h is < 1 hour 
              m = int(r/60) # returns minutes 
              s = r%60 # returns the rest if m < 1 minute
              age_msg = (str(age_msg) + "~" + str(d) + "d" + str(h) + "h" + str(m) + "m" + str(s) + "s" )
          return (age_msg) # return age specifying days, hours, minutes and seconds
              
     def is_i_sent(self): #TO TEST what type of message it is 
         return (self.type) == Message.MSG_ISENT
     def is_i_received(self):
         return (self.type) == Message.MSG_IRECEIVED
     def is_they_pos(self):
         return (self.type) == Message.MSG_POSITIVE
          
     def set_type(self, new_type):
         self.type = new_type #this function if used, sets/creates/adds a new type of message called new_type
    
	 #a class method that generates a random message
     @classmethod
     def generate_random_msg(cls):
         return sha256(bytes(str(time.time()) + str(random()), "utf8" )).hexdigest()
         #this creates a SHA-256 hash object by giving as input some bytes. These bytes are coming from
         #a string containing a random message, its date/age in utf8.
     
	 #a method to convert the object to string data
     def __str__(self): #useful for debugging
         return f"\t\t<message type=\"{self.type}\">\n\t\t\t<content>{self.content}</content>\n\t\t\t<date>{self.hdate()}</date>\n\t\t</message>"
         
	 #export the elements of the objects (message/new mwssage or updated msg, every time)
     def m_export(self):
         return f"{{\"content\":\"{self.content}\",\"type\":{self.type}, \"date\":{self.date}}}"

     def m_import(self, msg): #to import the object, to memorize it as json file, everytime, or uptade it everytime
        if (type(msg) == type(str())): #if the objct type is a string, the app can access it by using json.loads
            json_object = json.loads(msg)
        elif (type(msg) == type(dict())):#else if the type doesn't belong to a stored type, then the object of
                                        # of the content is assigned to json_object
            json_object = msg
        else :
         raise ValueError #if the object content is not compatible then we create an error message 
        self.content = json_object["content"]#assigning an element of the object called "content" to the vriable content
        self.type = json_object["type"]#assigning an element of the object called "type" to the vriable type
        self.data = json_object["data"]#assigning an element of the object called "data" to the vriable data
        #will only execute if this file is run
        #code just for testing, itis just to give inputs that we want to give to see if these are passed into 
        #the functions we made and tetrun a value
        if __name__ == "__main__":
                myMessage = Message("hi") #test the class: for example if we want to say "hi"
                time.sleep(1)
                mySecondMessage = Message(Message.generate_random_msg(), Message.MSG_POSITIVE)
                copyOfM = Message(myMessage.m_export())
                print(myMessage)
                print(mySecondMessage)
                print(copyOfM)
                time.sleep(0.5)
                print(copyOfM.age(True, True))
                time.sleep(0.5)
                print(copyOfM.age(False,True))
