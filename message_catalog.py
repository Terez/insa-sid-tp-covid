import json, os
from message import *

class MessageCatalog: #we create the class MessageCatalog
	#constructor, loads the messages (if there are any)
	#and we store them  in a json file
	def _init_(self, filepath):
		#            I SENT,I RECEIVED,POSITIVE
		self.data = [  []  ,  []   , [] ]
		self.filepath = filepath
		if os.path.exists(self.filepath): #if there are messages we open the json file
			self.open()
		else:            #otherwise 
			self.save()

	#destructor closes file.
	def _del_(self):
		del self.filepath
		del self.data

	#get catalog size: res is the sum of all the messages (#n. type I SENT + #n. I RECEIVED + #n.  POSITIVE)
	def get_size(self, msg_type=False):
		if(msg_type is False):
			res = self.get_size(Message.MSG_ISENT) + self.get_size(Message.MSG_IRECEIVED) + self.get_size(Message.MSG_POSITIVE)
		else:
			res = len(self.data[msg_type]) #otherwise how many dates (messages attributes)
		return res

	#Import object content
	def c_import(self, content, save=True):
		i = 0
		for msg in content:
			#we don't save after adding message because we just
			#read the content
				if self.add_message(Message(msg), False): #we looking for a messages in the object content
					i=i+1
		if save:
			self.save()
		return i

	#Import a set of messages but changing their type to new_type
	def c_import_as_type(self, content, new_type=Message.MSG_POSITIVE):
		i=0
		for msg in content:
			m = Message(msg)
			m.set_type(new_type)
			if self.add_message(m, False):
				i=i+1
		if i > 0:
			self.save() #we save a new message type if add_message is False 
		return i

	
	def open(self):                    #Open = load from file
		file = open(self.filepath,"r") # we are opening  an existing file for reading if there is no file, then it throws an error.
		self.c_import(json.load(file), False)
		file.close()

	#export the messages of a given type from a catalog
	def c_export_type(self, msg_type, indent=2, bracket=True):
		tmp = int(bracket) * "[\n"
		first_item = True
		for msg in self.data[msg_type]:
			if first_item:
				first_item=False
			else:
				tmp = tmp +",\n"
			tmp = tmp + (indent*" ") +  msg.m_export()
		tmp = tmp + int(bracket) * "\n]"
		return tmp

	#Export the whole catalog under a text format
	def c_export(self, indent=2):
		tmp = ""
		if(len(self.data[Message.MSG_ISENT])> 0 ): #if more the one message was sent 
			tmp = tmp + self.c_export_type(Message.MSG_ISENT, indent, False) #we esport the full message
		if(len(self.data[Message.MSG_IRECEIVED]) > 0): #if more the one message was received 
			if tmp != "":                              # if it is not a string
				tmp = tmp + ",\n"                      # we add the messages (different type) togheter, separated by "," and next space 
			tmp = tmp + self.c_export_type(Message.MSG_IRECEIVED, indent, False)
		if(len(self.data[Message.MSG_POSITIVE]) > 0):  #if more the one message was positive
			if tmp != "":                              # if it is not a string
				tmp = tmp + ",\n"                      # we add the messages (different type) togheter, separated by "," and next space
			tmp = tmp + self.c_export_type(Message.MSG_POSITIVE, indent, False)
		tmp = "[" + tmp + "\n]"   #the full catalog
		return tmp

	#a method to convert the object to string data
	def _str_(self): #we are taking the element values from the json file and
		tmp="<catalog>\n" #converting them into strings and placing them per messages type separated by a row and a comma
		tmp=tmp+"\t<isent>\n"
		for msg in self.data[Message.MSG_ISENT]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</isent>\n\t<ireceived>\n"
		for msg in self.data[Message.MSG_IRECEIVED]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</ireceived>\n\t<they_said_positive>\n"
		for msg in self.data[Message.MSG_POSITIVE]:
			tmp = tmp+str(msg)+"\n"
		tmp=tmp+"\n\t</they_said_positive>\n</catalog>"
		return tmp

	#Save object content to file
	def save(self):
		file = open(self.filepath,"w") #we are opening a Python file in write mode and overriding the existing content with new content. 
		file.write(self.c_export())    # It creates a new file if the file doesn’t exist.
		file.close()
		return True

	#add a Message object to the catalog
	def add_message(self, m, save=True): #we are checking if a messages is already in the catalog 
		res = True                       # if not we save it (append(m))
		if(self.check_msg(m.content, m.type)):
			print(f"{m.content} is already there")
			res = False
		else:
			self.data[m.type].append(m)
			if save:
				res = self.save()
		return res

	# remove all messages of msg_type that are older than max_age days
	def purge(self, max_age=14, msg_type=False):
		if(msg_type is False):
			self.purge(max_age, Message.MSG_ISENT)
			self.purge(max_age, Message.MSG_IRECEIVED)
			self.purge(max_age, Message.MSG_POSITIVE)
		else:              #we are removing the messages types that are older 14 days and  
			removable = [] #we are appending them in removable
			for i in range(len(self.data[msg_type])):
				if(self.data[msg_type][i].age(True)>max_age):
					removable.append(i)
			while len(removable) > 0:    #we eliminate the "pile" one at a time 
					del self.data[msg_type][removable.pop()]
			self.save()
		return True

	#Check if message string is in a category
	def check_msg(self, msg_str, msg_type=Message.MSG_POSITIVE): #we want to check if a message is in the hospital catalog (matching content)
		for msg in self.data[msg_type]:
			if msg_str == msg.content:
				return True
		return False

	#Say if I should quarantine based on the state of my catalog
	def quarantine(self, max_heard=4): 
		self.purge()
		n = 0
		for msg in self.data[Message.MSG_IRECEIVED]:
			if self.check_msg(msg.content): #if the messages received match the hospital catalog i should quarantine
				n = n + 1
		print(f"{n} covid messages received")
		return max_heard < n

#will only execute if this file is run
if _name_ == "_main_":
	#test the clas
	catalog = MessageCatalog("test.json")
	catalog.add_message(Message())
	catalog.add_message(Message(Message.generate(), Message.MSG_IRECEIVED))
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message(f"{{\"content\":\"{Message.generate()}\",\"type\":{Message.MSG_POSITIVE}, \"date\":{time.time()}}}"))
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(0.5)
	catalog.add_message(Message())
	print(catalog.c_export())
	time.sleep(2)
	catalog.purge(2)
	print(catalog)
