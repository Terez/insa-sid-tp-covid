#!/usr/bin/env python3
def falsifytime():
	import time as time
	#the referencetime is when we load this module
	referencetime = time.time()
	#factor is how many seconds of fake time correspond to a real second
	factor = 3600*24*14/60 #one real minute is fourteen days of fake time

	#override time.time()
	real_time = time.time
	def fake_time():
		duration = real_time() - referencetime
		return referencetime + factor*duration
	time.time = fake_time
	return time
